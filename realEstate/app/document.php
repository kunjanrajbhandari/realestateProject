<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class document extends Model
{
    protected $table= 'files';
    protected $fillable = ['name'];

}
