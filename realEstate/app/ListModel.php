<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ListModel extends Model
{
     protected $table= 'addlist';
     protected $fillable = ['name','email','phone_number','no_of_rooms','space','description','image','cash'];
}
