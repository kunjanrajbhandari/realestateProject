<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ListModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $lists = ListModel::get();
        return view('index',compact('lists'));
        //return view('index', ['index' => $index]);
    }

   

    // public function jpt(){
    //     $users= Auth::users()->find($id);
    //     return view('jpt');
    // }

    public function addlist(){
        return view('addlist');
    }
    public function contact(){
        return view('contact');
    }
}
