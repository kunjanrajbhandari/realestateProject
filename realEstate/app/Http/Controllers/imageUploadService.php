<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class imageUploadService{

    public function imageUpload(Request $request){
        // request()->validate([

        //     'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        // ]);

  

        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('img'), $imageName);
        return $imageName;

    }

}