<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ListModel;
//use App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Contact;

class listcontroller extends Controller
{

    protected $imageUploadService;
    protected $addPropertyService;
        public function __construct(imageUploadService $imageUploadService, addPropertyService $addPropertyService){
            $this->imageUploadService = $imageUploadService;
            $this->addPropertyService = $addPropertyService;
        }


        

        public function addlist(){
            return view('addlist');
        }
        public function savelist(Request $request){
            $list = new ListModel();
            
                
            $list->name = Auth::user()->name;
            $list->email = Auth::user()->email;
            $list->phone_number = $request->input('phone_number');
            $list->no_of_rooms = $request->input('no_of_rooms');
            $list->space = $request->input('space');
            $list->cash = $request->input('cash');
            $list->description = $request->input('description');
            $list->image = $this->imageUploadService->imageUpload($request);
        
             $list->save();
    
            return redirect()->route('home');



        }

        public function showFeatureList(){
            $users = ListModel::all();
            return view('feature_list',compact('users'));
        }
        
        public function showcontact(){
            return view('contact');
        }

        public function savecontact(Request $request){
           
            $contact = new contact();
          
            $contact->auth_id=Auth::user()->id;
            $contact->message = $request->input('message');
            $contact->save();
            return redirect()->route('home');
        }
      
        public function property(){
            return view('addProperty');
        }
       

}
