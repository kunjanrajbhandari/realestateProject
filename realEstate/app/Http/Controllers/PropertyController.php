<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\add_property;

class PropertyController extends Controller
{
      
    protected $addPropertyService;
    public function __construct( addPropertyService $addPropertyService){
           
        $this->addPropertyService = $addPropertyService;
    }

    public function save_property(Request $request){
        $this->addPropertyService->addProperty($request->all());
    }

    
    public function property(){
        return view('addProperty');
    }
}
