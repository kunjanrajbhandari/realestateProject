<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class add_property extends Model
{
    protected $table= 'addProperty';
    protected $fillable = ['phone_number','no_of_rooms','location','state','land_area','description','image','cash'];
}
