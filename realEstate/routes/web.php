<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/contact', 'HomeController@contact')->name('contact');
//Route::get('/jpt', 'HomeController@jpt')->name('jpt');
Route::get('/addlist','listcontroller@addlist')->name('addlist');
Route::post('addlist','listcontroller@savelist')->name('savelist')->middleware('auth');

Route::get('feature_list','listcontroller@showFeatureList')->name('showFeatureList');

Route::get('file', 'uploadController@index');
Route::post('save', 'uploadController@save')->name('file');
Route::get('contact', 'PropertyController@showcontact')->name('contact');

Route::get('contac', 'listController@savecontact')->name('savecontact')->middleware('auth');

Route::get('addProperty', 'PropertyController@property')->name('property');
//Route::get('addProperty', 'listController@property');

Route::post('add_propert', 'PropertyController@save_property')->name('saveProperty');