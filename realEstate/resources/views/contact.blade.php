@extends('layouts.layout')
@section('content')
<!-- page -->
	<section class="page-section blog-page">
		<div class="container">
			
			<div class="contact-info-warp">
				<p><i class="fa fa-map-marker"></i>Maitidevi, Kathmandu</p>
				<p><i class="fa fa-envelope"></i>rbkunjan@gmail.com</p>
				<p><i class="fa fa-phone"></i>(+977) 9819087207 </p>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<img src="img/contact.jpg" alt="">
				</div>
				<div class="col-lg-6">
					<div class="contact-right">
						<div class="section-title">
							<h3>Get in touch</h3>
							<p>Browse houses and flats for sale and to rent in your area</p>
						</div>
						<form class="contact-form" action={{route('savecontact')}}>
							<div class="row">
								
							
								<div class="col-md-12">
									<textarea  placeholder="Your message" name="message"></textarea>
									<button class="site-btn">SUMMIT NOW</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- page end -->


	<!-- Clients section -->
	<div class="clients-section">
		<div class="container">
			<div class="clients-slider owl-carousel">
				<a href="#">
					<img src="img/partner/1.png" alt="">
				</a>
				<a href="#">
					<img src="img/partner/2.png" alt="">
				</a>
				<a href="#">
					<img src="img/partner/3.png" alt="">
				</a>
				<a href="#">
					<img src="img/partner/4.png" alt="">
				</a>
				<a href="#">
					<img src="img/partner/5.png" alt="">
				</a>
			</div>
		</div>
	</div>
	<!-- Clients section end -->
@endsection