<!DOCTYPE html>
<html lang="en">
<head>
	<title>Real Estate</title>
	<meta charset="UTF-8">
	<meta name="description" content="LERAMIZ Landing Page Template">
	<meta name="keywords" content="LERAMIZ, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/animate.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/style.css')}}"/>


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>
	
	<!-- Header section -->
	<header class="header-section">
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 header-top-left">
						<div class="top-info">
							<i class="fa fa-phone"></i>
							(+977) 9819087207
						</div>
						<div class="top-info">
							<i class="fa fa-envelope"></i>
							rbkunjan@gmail.com
						</div>
					</div>
					<div class="col-lg-6 text-lg-right header-top-right">
						<div class="top-social" id="app">
						<nav class="navbar navbar-expand-md navbar-light bg-trans" >
						<div class="container">
							<a href=""><i class="fa fa-facebook"></i></a>
							<a href=""><i class="fa fa-twitter"></i></a>
							<a href=""><i class="fa fa-instagram"></i></a>
							<a href=""><i class="fa fa-pinterest"></i></a>
							<a href=""><i class="fa fa-linkedin"></i></a>
						



					

								
									
											{{-- <a class="navbar-brand" href="{{ url('/') }}">
												{{ config('app.name', 'Laravel') }}
											</a> --}}
											{{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
												<span class="navbar-toggler-icon"></span>
											</button> --}}

											<div class="collapse navbar-collapse" id="navbarSupportedContent">
												<!-- Left Side Of Navbar -->
												<ul class="navbar-nav mr-auto">

												</ul>

												<!-- Right Side Of Navbar -->
												<ul class="navbar-nav ml-auto">
													<!-- Authentication Links -->
													@guest
														<li class="nav-item">
															<a class="nav-link" style="color:white;" href="{{ route('login') }}">{{ __('Login') }}</a>
														</li>
														@if (Route::has('register'))
															<li class="nav-item">
																<a class="nav-link" style="color:white;" href="{{ route('register') }}">{{ __('Register') }}</a>
															</li>
														@endif
													@else
														<li class="nav-item dropdown" >
															<a id="navbarDropdown" class="nav-link dropdown-toggle" style="color:white;" style="float:right;"  href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
																{{ Auth::user()->name }} <span class="caret"></span>
															</a>

															<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"  >
																<a class="dropdown-item" style="color:black;"  href="{{ route('logout') }}"
																onclick="event.preventDefault();
																				document.getElementById('logout-form').submit();">
																	{{ __('Logout') }}
																</a>

																<form id="logout-form" action="{{ route('logout') }}" method="POST" style="color:black; ">
																	@csrf
																</form>
															</div>
														</li>
													@endguest
												</ul>
											</div>
										
									</nav>
						</div>









						{{-- <div class="user-panel">
							<a href="register"><i class="fa fa-user-circle-o"></i> Register</a>
							<a href="login"><i class="fa fa-sign-in"></i> Login</a>
						</div> --}}
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="navbar">
					<div class="site-navbar">
						<a href="#" class="site-logo"><img src="img/realestate.png" alt=""></a>
						<div class="nav-switch">
							<i class="fa fa-bars"></i>
						</div>

						<ul class="main-menu">
							<div class="navbar">
								<li><a href={{route('home')}}>Home</a></li>
									
									<li class="dropdown">
										<a  data-toggle="dropdown" role="button" aria-expanded="false">Add listing<span class="caret"></span></a>
										<a class="dropdown-menu" role="menu" href={{route('property')}}>Add property</a>
									</li>

							<li><a href={{route('addlist')}}>Add Listing</a></li>
							<li><a href={{route('showFeatureList')}}>FEATURED LISTING</a></li>
							
							
							<li><a href={{route('contact')}}>Contact</a></li>
						<div>
						</ul>
					
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- Header section end -->
	


	<!-- Hero section -->
	<section class="hero-section set-bg mb-5" data-setbg="img/bg.jpg">
		<div class="container hero-text text-white">
			<h2>find your place with our local life style</h2>
			<p>Search real estate property records, houses, condos, land and more on leramiz.com®.<br>Find property info from the most comprehensive source data.</p>
			<a href="#" class="site-btn">VIEW DETAIL</a>
		</div>
	</section>
	<!-- Hero section end -->


	















	
		
	<!-- Filter form section end -->
