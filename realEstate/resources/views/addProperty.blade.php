
<!DOCTYPE html>
<html lang="en">
<head>
@extends('layouts.layout')
@section('content')

    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    <title>Add list</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
    .send-button{
background: #54C7C3;
width:100%;
font-weight: 600;
color:#fff;
padding: 8px 25px;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.g-button{
color: #fff !important;
border: 1px solid #EA4335;
background: #ea4335 !important;
width:100%;
font-weight: 600;
color:#fff;
padding: 8px 25px;
}
.my-input{
box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
cursor: text;
padding: 8px 10px;
transition: border .1s linear;
}
.header-title{
margin: 5rem 0;
}
h1{
font-size: 31px;
line-height: 40px;
font-weight: 600;
color:#4c5357;
}
h2{
color: #5e8396;
font-size: 21px;
line-height: 32px;
font-weight: 400;
}
.login-or {
position: relative;
color: #aaa;
margin-top: 10px;
margin-bottom: 10px;
padding-top: 10px;
padding-bottom: 10px;
}
.span-or {
display: block;
position: absolute;
left: 50%;
top: -2px;
margin-left: -25px;
background-color: #fff;
width: 50px;
text-align: center;
}
.hr-or {
height: 1px;
margin-top: 0px !important;
margin-bottom: 0px !important;
}
@media screen and (max-width:480px){
h1{
font-size: 26px;
}
h2{
font-size: 20px;
}
}    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
            else $('head > link').filter(':first').replaceWith(defaultCSS); 
        }
        $( document ).ready(function() {
          var iframe_height = parseInt($('html').height()); 
          window.parent.postMessage( iframe_height, 'https://bootsnipp.com');
        });
    </script>
</head>
<body>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<body>
   <div class="container">
      <div class="col-md-6 mx-auto text-center">
         <div class="header-title">
            <h1 class="wv-heading--title">
               Check out — it’s free!
            </h1>
            <h2 class="wv-heading--subtitle">
               Lorem ipsum dolor sit amet! Neque porro quisquam est qui do dolor amet, adipisci velit...
            </h2>
         </div>
      </div>
      <div class="row">
         <div class="col-md-4 mx-auto">
            <div class="myform form ">
               <form action="{{route('saveProperty')}}" method="post" name="login" enctype="multipart/form-data">
               {{csrf_field()}}
               <div class="form-group">
                    <input type="file" class="form-control-file" name="file" id="file" aria-describedby="fileHelp" >
                   
                </div>

                  {{-- <div class="form-group">
                     <input type="text" name="name"  class="form-control my-input" id="name" placeholder="Name">
                  </div> --}}
                  {{-- <div class="form-group">
                     <input type="email" name="email"  class="form-control my-input" id="email" placeholder="Email">
                  </div> --}}
                  
                  <div class="form-group">
                     <input type="text" name="phone_number"  class="form-control my-input" id="phone" placeholder="Phone Number">
                  </div>

                   <div class="form-group">
                     <input type="text" name="number_of_rooms"  class="form-control my-input" id="no_of_rooms" placeholder="number of rooms">
                  </div>

                  
                  
                   <div class="form-group">
                     <input type="loc" name="location"  class="form-control my-input" id="location" placeholder="location">
                  </div>

                   <div class="form-group">
                     <input type="text" name="state"  class="form-control my-input" id="state" placeholder="state">
                  </div>

                   <div class="form-group">
                     <input type="text" name="land_area"  class="form-control my-input" id="land_area" placeholder="land_area">
                  </div>

                  <div class="form-group">
                     <input type="text" name="cash"  class="form-control my-input" id="cash" placeholder="Cash">
                  </div>


                  <div id="description" class="form-row">
                    <textarea name = "description" placeholder = "description" id = "description" ></textarea>
                  </div><br>

                  <div class="text-center ">
                     <button type="submit" class=" btn btn-block send-button tx-tfm">Save</button>
                  </div>
                  <br><br>
                  
               </form>
            </div>
         </div>
      </div>
   </div>
</body>	<script type="text/javascript">
		</script>
</body>
@endsection
</html>
