@extends('layouts.layout')
@section('content')

<section class="page-section categories-page mt-5">
       
		<div class="container">
		<div class="row">
			@foreach($users as $list)
			 
				<div class="col-lg-4 col-md-6">
                {{-- @foreach($lists as $list) --}}
					<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="{{asset('img/' . $list->image) }}" >
				     		{{-- photo --}}
							<div class="sale-notic">FOR SALE</div>
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5>1963 S Crescent Heights Blvd</h5>           {{-- location --}}
								<p><i class="fa fa-map-marker"></i> Los Angeles, CA 90034</p>   {{-- location --}}
							</div>
							<div class="room-info-warp">
								<div class="room-info">
									<div class="rf-left">
										<p><i class="fa fa-th-large"></i>{{ $list->space }}Square ft.</p>       {{-- space --}}
										<p><i class="fa fa-bed"></i> {{ $list->no_of_rooms }} Rooms</p>                 {{-- no of rooms  --}}
									</div>
									
								</div>
								
							</div>
						</div>
							<a href="#" class="room-price">Rs. {{ $list->cash }} </a>           {{-- cash    --}}
					</div>
				</div>
                    
			@endforeach
			</div>
		<div class="site-pagination">
				<span>1</span>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#"><i class="fa fa-angle-right"></i></a>
		</div>
	</div>
</section>
  
	<!-- page end -->


	<!-- Clients section -->
	{{-- <div class="clients-section">
		<div class="container">
			<div class="clients-slider owl-carousel">
				<a href="#">
					<img src="img/partner/1.png" alt="">
				</a>
				<a href="#">
					<img src="img/partner/2.png" alt="">
				</a>
				<a href="#">
					<img src="img/partner/3.png" alt="">
				</a>
				<a href="#">
					<img src="img/partner/4.png" alt="">
				</a>
				<a href="#">
					<img src="img/partner/5.png" alt="">
				</a>
			</div>
		</div>
	</div> --}}
@endsection