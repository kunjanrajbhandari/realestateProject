@extends('layouts.layout')
@section('content')

<!-- Filter form section -->
	<div class="filter-search">
		<div class="container">
			<form class="filter-form">
				<input type="text" placeholder="Enter a street name, address number or keyword">
				<select>
					<option value="City">City</option>
				</select>
				<select>
					<option value="City">State</option>
				</select>
				<button class="site-btn fs-submit">SEARCH</button>
			</form>
		</div>
	</div>


		<!-- Services section -->
	<section class="services-section spad set-bg" data-setbg="img/service-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<img src="img/service.jpg" alt="">
				</div>
				<div class="col-lg-5 offset-lg-1 pl-lg-0">
					<div class="section-title text-white">
						<h3>OUR SERVICES</h3>
						<p>We provide the perfect service for </p>
					</div>
					<div class="services">
						<div class="service-item">
							<i class="fa fa-comments"></i>
							<div class="service-text">
								<h5>Consultant Service</h5>
								<p>In Aenean purus, pretium sito amet sapien denim consectet sed urna placerat sodales magna leo.</p>
							</div>
						</div>
						<div class="service-item">
							<i class="fa fa-home"></i>
							<div class="service-text">
								<h5>Properties Management</h5>
								<p>In Aenean purus, pretium sito amet sapien denim consectet sed urna placerat sodales magna leo.</p>
							</div>
						</div>
						<div class="service-item">
							<i class="fa fa-briefcase"></i>
							<div class="service-text">
								<h5>Renting and Selling</h5>
								<p>In Aenean purus, pretium sito amet sapien denim consectet sed urna placerat sodales magna leo.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Services section end -->

	<!-- Breadcrumb -->
	<div class="site-breadcrumb">
		<div class="container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Featured Listings</span>
		</div>
	</div>


	<!-- page -->


<section class="page-section categories-page mt-5">
       
		<div class="container">
		<div class="row">
		
			@foreach($lists as $list)
			 
				<div class="col-lg-4 col-md-6">
                {{-- @foreach($lists as $list) --}}
				 		<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="{{asset('img/' . $list->image) }}" >
				     		{{-- photo --}}
							<div class="sale-notic">FOR SALE</div>
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5>1963 S Crescent Heights Blvd</h5>           {{-- location --}}
								<p><i class="fa fa-map-marker"></i> Los Angeles, CA 90034</p>   {{-- location --}}
							</div>
							<div class="room-info-warp">
								<div class="room-info">
									<div class="rf-left">
										<p><i class="fa fa-th-large"></i>{{ $list->space }}Square ft.</p>       {{-- space --}}
										<p><i class="fa fa-bed"></i> {{ $list->no_of_rooms }} Rooms</p>                 {{-- no of rooms  --}}
									</div>
									
								</div>
								
							</div>
						</div>
							<a href="#" class="room-price">Rs. {{ $list->cash }} </a>           {{-- cash    --}}
					</div>
				</div>
                    
			@endforeach
			</div>
		<div class="site-pagination">
				<span>1</span>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#"><i class="fa fa-angle-right"></i></a>
		</div>
	</div>
</section>
	


@endsection
