<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addlist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addlist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->mediumText('image')->nullable()->default('default.png');
            $table->string('name');

            $table->integer('phone_number')->unique();
            $table->timestamps();
            $table->integer('no_of_rooms');
           // $table->timestamp('email_verified_at')->nullable();
            $table->string('space');
            $table->string('description');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addlist');
    }
}
