<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addProperty', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('auth_id');
            $table->string('phone_number');
            $table->string('numer_of_rooms');
            $table->string('location');
            $table->string('state');
            $table->string('land_area');
            $table->string('cash');

            $table->string('description');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('addProperty');
    }
}
